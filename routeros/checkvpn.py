# checkvpn.py
#
# Interfaz para interactuar con un secret y su interfaz vpn asociada.
# Copyright (C) 2020  William Ernesto Cárdenas Gómez <willecg@cpapaseo.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
from gi.repository import Gtk, GObject

@Gtk.Template(resource_path='/com/cpapaseo/linbox/checkvpn.ui')
class CheckVPN(Gtk.Box):
    """Interfaz que interactua con una vpn (secret e interfaz vpn)"""
    __gtype_name__ = 'CheckVPN'
    __gsignals__ = {
            'change_st' : (GObject.SIGNAL_RUN_FIRST, None, (str, bool))
            }

    im_interfaz = Gtk.Template.Child()
    ch_secret = Gtk.Template.Child()

    def __repr__(self):
        return '<CheckVPN>'

    def __init__(self, nombre, habilitada, activa):
        """Constructor"""
        super(CheckVPN, self).__init__()
        self.nombre = nombre
        self.habilitada = habilitada
        self.activa = activa
        self.ch_secret.connect('toggled', self.on_ch_secret_toggled)

    @GObject.Property(type=str, default='Vacío')
    def nombre(self):
        """Obtiene etiqueta del widget.
        
        :return: Texto de la etiqueta del widget
        :rtype: String"""
        return self.ch_secret.get_label()

    @nombre.setter
    def nombre(self, nombre):
        """Establece el texto de la etiqueta del widget.

        :param nombre: texto de la etiqueta del widget.
        :type nombre: String"""
        self.ch_secret.set_label(nombre)

    @GObject.Property(type=bool, default=False)
    def habilitada(self):
        return self.ch_secret.get_active()

    @habilitada.setter
    def habilitada(self, habil):
        self.ch_secret.set_active(habil)

    @GObject.Property(type=bool, default=False)
    def activa(self):
        return self._activa

    @activa.setter
    def activa(self, activa):
        if activa:
            self.im_interfaz.set_from_icon_name('gtk-connect',
                Gtk.IconSize.MENU)
        else:
            self.im_interfaz.set_from_icon_name('gtk-disconnect',
                Gtk.IconSize.MENU)
        self._activa = activa

    def on_ch_secret_toggled(self, widget):
        self.emit('change_st', widget.props.label, widget.props.active)
