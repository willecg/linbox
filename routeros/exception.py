# exception.py
# Exception handling.
#
# Copyright (C) 2019  William Ernesto Cárdenas Gómez <willecg@cpapaseo.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
class ROError(Exception):
    """Excepción padre del módulo"""
    def __init__(self, msg=None, detail=None):
        super().__init__()
        self.msg = msg
        self.detail = detail


class LoginError(ROError):
    """Errores de inicio de sesión"""
    def __init__(self, msg=None, detail=None):
        super().__init__(msg, detail)


class RetriveError(ROError):
    """Errores al solicitar datos del router"""
    def __init__(self, msg=None, detail=None):
        super().__init__(msg, detail)


class SettingError(ROError):
    """Errores al agregar o modificar algún valor del router"""
    def __init__(self, msg=None, detail=None):
        super().__init__(msg, detail)
