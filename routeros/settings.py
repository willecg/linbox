# settings.py
#
# Control the gsettings values.
# Copyright (C) 2019  William Ernesto Cárdenas Gómez <willecg@cpapaseo.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from gi.repository import Gio

class Settings(Gio.Settings):


    def __init__(self):
        super().__init__('com.cpapaseo.linbox')


    @property
    def maximizado(self):
        return self.get_boolean('ismaximized')


    @maximizado.setter
    def maximizado(self, valor):
        self.set_boolean('ismaximized', valor)


    @property
    def anchowin(self):
        return self.get_int('currentwidht')


    @anchowin.setter
    def anchowin(self, valor):
        self.set_int('currentwidht', valor)


    @property
    def altowin(self):
        return self.get_int('currentheigth')

    
    @altowin.setter
    def altowin(self, valor):
        self.set_int('currentheigth', valor)
