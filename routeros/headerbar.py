# headerbar.py
#
# Barra de encabezado de la ventana.
# Copyright (C) 2020  William Ernesto Cárdenas Gómez <willecg@cpapaseo.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
from gi.repository import Gtk, GObject
from routeros.enums import Estados

@Gtk.Template(resource_path='/com/cpapaseo/linbox/headerbar.ui')
class HeaderBar(Gtk.HeaderBar):
    """Barra de encabezado de la aplicación"""
    __gtype_name__ = 'HeaderBar'
    __gsignals__ = {
            'on_entrar' : (GObject.SIGNAL_RUN_FIRST, None, ()),
            'on_disconnect': (GObject.SIGNAL_RUN_FIRST, None, ()),
            'on_connect': (GObject.SIGNAL_RUN_FIRST, None, ()),
            }

    b_entrar = Gtk.Template.Child()
    b_menu = Gtk.Template.Child()
    bbox_connection = Gtk.Template.Child()
    b_disconnect = Gtk.Template.Child()
    b_connect = Gtk.Template.Child()

    def __repr__(self):
        return 'HeaderBar'

    def __init__(self):
        super().__init__()
        builder = Gtk.Builder.new_from_resource(
                '/com/cpapaseo/linbox/menu.ui')
        self.b_menu.set_menu_model(builder.get_object('menu-primario'))

    @GObject.Property(type=int, default=int(Estados.LOGIN))
    def estado(self):
        """Obtiene la propiedad conectado

        :return: Propiedad conectado
        :rtype: Bool"""
        return self._estado

    @estado.setter
    def estado(self, estado):
        """Establece la propiedad conectado

        :param conectado: Indica si muestra la interfaz de inicio de sesión o
        la de operación.
        :type conectado: Boolean"""
        self._estado = estado
        if estado == Estados.CONECTADO:
            self.props.subtitle = 'Listado de VPNs'
            self.bbox_connection.set_visible(True)
        elif estado == Estados.PROCESANDO:
            self.props.subtitle = 'Espere por favor'
            self.bbox_connection.set_visible(False)
        else:
            self.props.subtitle = 'Inicio de sesión'
            self.bbox_connection.set_visible(False)
        if estado == Estados.LOGIN:
            self.b_entrar.set_visible(True)
            self.bbox_connection.set_visible(False)
        else:
            self.b_entrar.set_visible(False)

    @Gtk.Template.Callback()
    def on_b_entrar_clicked(self, widget):
        self.emit('on_entrar')

    @Gtk.Template.Callback()
    def on_disconnect(self, widget):
        self.emit('on_disconnect')

    @Gtk.Template.Callback()
    def on_connect(self, widget):
        self.emit('on_connect')
