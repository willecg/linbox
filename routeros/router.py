# router.py
#
# Copyright (C) 2020  William Ernesto Cárdenas Gómez <willecg@cpapaseo.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import routeros_api
from routeros_api.exceptions import RouterOsApiError
from routeros_api.exceptions import RouterOsApiConnectionError
from routeros_api.exceptions import RouterOsApiCommunicationError

from routeros.exception import LoginError

from threading import Lock


class Router:
    """Connection and administration of routeros device.

    Wraps and hide the implementation of routeros_api
    
    ..autoclass::
        :type user: String.
        :param user: User for login operation.
        :type password: String.
        :param password: Password for login operation.
        :param host: Host to connect.
        :type host: String"""

    def __repr__(self):
        return '<Router>'

    def __init__(self, user, password, host):
        self.connection = None
        self.api = None
        self.secrets = None
        self.lock = Lock()
        try:
            self.connection = routeros_api.RouterOsApiPool(host, user, password,
                                                   plaintext_login=True)
            self.api = self.connection.get_api()
        except RouterOsApiConnectionError as er:
            self.close()
            print(er)
            raise LoginError("Error al conectar a " + host)
        except RouterOsApiCommunicationError as er:
            self.close()
            print(er)
            raise LoginError("Error de inicio de sesión", str(er))
        except RouterOsApiError as er:
            self.close()
            print(er)
            raise ROError("Error de Api", str(er))
        except Exception as er:
            self.close()
            print(er)
            raise ROError("Error genérico", str(er))

    def get_list(self):
        """ Get the list of vpns connecteds.

        :return: List of dicts of vpns.
        :rtype: List of vpns."""
        self.lock.acquire()
        try:
            secrets = self.api.get_resource('/ppp/secret/').get()
            actives = self.api.get_resource('/ppp/active/').get()
            listado = []
            for i in secrets:
                founded = False
                for j in actives:
                    if j['name'] == i['name']:
                        founded = True
                        break
                item = {
                    'name': i['name'],
                    'disabled': i['disabled'],
                    'active': founded,
                    }
                listado.append(item)
            return listado
        except Exception as er:
            self.close()
            print(er)
            self.lock.release()
            raise RetriveError("Error de comunicación", er)
        finally:
            self.lock.release()

    def set_disable_vpn(self, name, disable=False):
        """Set the disable flag for an secret.

        :param name: The name of the secret.
        :type name: String.
        :param disable: True of False for the desired value.
        :type name: Boolean."""
        self.lock.acquire()
        try:
            disabled = 'no'
            if disable:
                disabled = 'yes'
            secrets = self.api.get_resource('/ppp/secret/')
            secret_id = secrets.get(name=name)[0]['id']
            secrets.set(id=secret_id, disabled=disabled)
            if disable:
                actives = self.api.get_resource('/ppp/active/')
                actives_list = actives.get()
                if len(actives_list):
                    active_id = None
                    for i in actives_list:
                        if i['name'] == name:
                            active_id = i['id']
                            print(i['name'] + ' - ' + i['id'] + ' - '
                                    + active_id)
                            break
                    if active_id:
                        actives.remove(id=active_id)
        except Exception as er:
            self.close()
            print(er)
            self.lock.release()
            raise SettingError("Error de cumunicación", er)
        finally:
            self.lock.release()

    def close(self):
        """ Closes the connection."""
        self.lock.acquire()
        try:
            self.connection.disconnect()
        except Exception as ex:
            print(ex)
        finally:
            self.lock.release()
