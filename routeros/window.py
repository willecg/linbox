# window.py
#
# Ventana principal de la aplicación.
# Copyright (C) 2020  William Ernesto Cárdenas Gómez <willecg@cpapaseo.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
from gi.repository import Gtk, GObject
from routeros.headerbar import HeaderBar
from routeros.boxlogin import LoginBox
from routeros.checkvpn import CheckVPN

from .router import Router
from routeros.exception import LoginError
from routeros.enums import Estados
from routeros.settings import Settings
import threading
import time

class EstadoVentana:
    def __init__(self):
        self.current_width = -1
        self.current_height = -1
        self.is_maximized = False

@Gtk.Template(resource_path='/com/cpapaseo/linbox/window.ui')
class ApplicationWindow(Gtk.ApplicationWindow):
    """Ventana principal de la aplicación"""
    __gtype_name__ = 'ApplicationWindow'
    
    stack = Gtk.Template.Child()
    listbox = Gtk.Template.Child()
    _scroll = Gtk.Template.Child()
    
    def __repr__(self):
        return '<Window>'

    def __init__(self, app):
        """Constructor"""
        super().__init__(application=app, title='Linbox')
        self._app = app
        self._settings = Settings()
        self._estadov = EstadoVentana()
        self._estadov.current_width = -1
        self._estadov.current_height = -1
        self._estadov.is_maximized = False
        self._headerbar = HeaderBar()
        self._loginbox = LoginBox()
        self.set_titlebar(self._headerbar)
        self.spinner = Gtk.Spinner()
        self.stack.add_named(self._loginbox, 'loginbox')
        self.stack.add_named(self._scroll, 'scroll')
        self.stack.add_named(self.spinner, 'Spinner')
        self.router = None
        self.listbox.set_sort_func(self._sort_func)
        self.show_all()
        self._headerbar.connect('on_entrar', self._entrar, None)
        self._headerbar.connect('on_disconnect', self._disconnect_all, None)
        self._headerbar.connect('on_connect', self._connect_all, None)
        self._estadov.current_width = self._settings.anchowin
        self._estadov.current_height = self._settings.altowin
        self._estadov.is_maximized = self._settings.maximizado
        self._flag_active = True
        self._vpns = []
        
        if not self._estadov.is_maximized:
            self.resize(self._estadov.current_width,
                                  self._estadov.current_height)
        self.connect('destroy', self.on_Window_destroy)
        if self._estadov.is_maximized:
            self.maximize()
        else:
            self.connect('size-allocate', self.on_Window_size_allocate)
        self.bind_property('estado',
            self._headerbar, 'estado',
            GObject.BindingFlags.BIDIRECTIONAL |
            GObject.BindingFlags.SYNC_CREATE)

    @GObject.Property(type=int, default=int(Estados.LOGIN))
    def estado(self):
        """Obtiene el estado de la aplicación

        :return: Estado de la aplicación
        :rtype: routeros.enums.Estados"""
        return self._estado
 
    @estado.setter
    def estado(self, estado):
        """Establece el estado de la ventana

        :param estado: Estado de la ventana.
        :type estado: routeros.enums.Estados"""
        self._estado = estado
        if estado == Estados.CONECTADO:
            self.stack.set_visible_child(self._scroll)
            self.spinner.stop()
            threading.Thread(target=self._run_update).start()
        elif estado == Estados.PROCESANDO:
            self.spinner.start()
            self.stack.set_visible_child(self.spinner)
        elif estado == Estados.CERRANDO:
            self.spinner.start()
            self._flag_active = False
        else:
            self.stack.set_visible_child(self._loginbox)
            self.spinner.stop()

    def _entrar(self, widget, params):
        """Start the process for login."""
        threading.Thread(target=self._inicia_sesion).start()
        self.props.estado = Estados.PROCESANDO

    def _connect_all(self, widget, params):
        """Enable all vpns"""
        self.listbox.select_all()
        self.listbox.selected_foreach(self._connect_all_vpns)
        self.listbox.unselect_all()


    def _disconnect_all(self, widget, params):
        """Disable all vpns"""
        self.listbox.select_all()
        self.listbox.selected_foreach(self._connect_all_vpns)
        self.listbox.unselect_all()

    def _connect_all_vpns(self, box, row):
        """Activate all vpns"""
        self.router.set_disable_vpn(row.get_children()[0].props.nombre, False)

    def _disconnect_all_vpns(self, box, row):
        """Disactivate all vpns"""
        self.router.set_disable_vpn(row.get_children()[0].props.nombre, True)

    def _inicia_sesion(self):
        """Performs the login operation."""
        dicc = {'usuario': self._loginbox.props.user,
                'password': self._loginbox.props.passwd,
                'host': self._loginbox.props.host,
                }
        try:
            self.router = Router(self._loginbox.user, self._loginbox.passwd,
                    self._loginbox.host)
            print('INFO: Conexión establecida a ' + self._loginbox.host)
            self._load_interface()
        except LoginError as ex:
            self.props.estado = Estados.LOGIN
            self._loginbox.props.errors = ex.msg
            self._loginbox.props.error_detail = ex.detail
            self._loginbox.show_error()
        except Exception as ex:
            self.props.estado = Estados.LOGIN
            self._loginbox.props.errors = 'Error de programación'
            self._loginbox.props.error_detail = str(ex)
            self._loginbox.show_error()
            raise ex
        else:
            self.props.estado = Estados.CONECTADO

    def _run_update(self):
        """Updates the list."""
        while self._flag_active:
            local_vpns_list = self.router.get_list()
            self._find_news(local_vpns_list)
            self._del_olds(local_vpns_list)
            self.vpns_list = local_vpns_list
            self.listbox.select_all()
            self.listbox.selected_foreach(self._update_row)
            self._changes = []
            self.listbox.unselect_all()
            time.sleep(1)
    
    def _update_row(self, box, row):
        """Updates a row."""
        check_vpn = row.get_children()[0]
        for i in self.vpns_list:
            if i['name'] == check_vpn.props.nombre:
                habilitada = False
                if i['disabled'] == 'false':
                    habilitada = True
                check_vpn.props.habilitada = habilitada
                check_vpn.props.activa = i['active']
                break

    def _find_news(self, local_vpns_list):
        """ Encontrar si hay nuevas vpns. """
        for i in self.vpns_list:
            for j in local_vpns_list:
                if i['name'] == j['name']:
                    break
            else:
                self._add_vpn(i)

    def _del_olds(self, local_vpns_list):
        """ Buscar si hay alguna que se eliminó. """
        for i in local_vpns_list:
            for j in self.vpns_list:
                if i['name'] == j['name']:
                    break
            else:
                self._vpn.remove(i)
                listado_de_chvpns = self.listbox.get_children()
                for i in listado_de_chvpns:
                    if i[0].nombre == i['name']:
                        i.listbox.remove(i)
                        break

    def _load_interface(self):
        """Load the interface of the main window."""
        try:
            self.vpns_list = self.router.get_list()
            for i in self.vpns_list:
                self._add_vpn(i)
                time.sleep(.15)
        except Exception:
            pass

    def _add_vpn(self, new_vpn):
        """ Add a new vpn to the list.

        :param new_vpn: Dictionary with new vpn data.
        :type new_vpn: Dict with values
            
            new_vpn = {
                    'name': String. Name of the vpn,
                    'active': Bool. True|False,
                    'disabled': String. Only 'false'|'true'
            }"""       
        habilitada = False
        if new_vpn['disabled'] == 'false':
            habilitada = True
        vpn = CheckVPN(new_vpn['name'], habilitada, new_vpn['active'])
        vpn.connect('change_st', self._on_connect_disconnect)
        self._vpns.append(new_vpn)
        self.listbox.prepend(vpn)

    def _on_connect_disconnect(self, widget, vpn, active):
        """ Capture the event 'change_st' """
        self.router.set_disable_vpn(vpn, not active)

    #Functions for listbox
    def _sort_func(self, row1, row2, *userdata):
        """Sets the order of the listbox"""
        rw1 = row1.get_children()[0].nombre.lower()
        rw2 = row2.get_children()[0].nombre.lower()
        if rw1 > rw2:
            return 1
        if rw1 == rw2:
            return 0
        return -1

    #Eventos de la ventana
    def on_Window_size_allocate(self, widget, allocation):
        self._estadov.current_width, self._estadov.current_height = \
                self.get_size()

    def on_Window_window_state_event(self, widget, event):
        self._estadov.is_maximized = self.is_maximized()
        return False

    def on_Window_destroy(self, widget):
        self._settings.anchowin = self._estadov.current_width
        self._settings.altowin = self._estadov.current_height
        self._estadov.is_maximized = self.is_maximized()
        self._settings.maximizado = self._estadov.is_maximized
        self.router.close()
        self._flag_active = False
