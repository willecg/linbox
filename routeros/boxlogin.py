# boxlogin.py
#
# Barra de encabezado de la ventana.
# Copyright (C) 2020  William Ernesto Cárdenas Gómez <willecg@cpapaseo.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
from gi.repository import Gtk, GObject
from threading import Thread
import time


@Gtk.Template(resource_path='/com/cpapaseo/linbox/boxlogin.ui')
class LoginBox(Gtk.Box):
    """Clase que realiza el proceso de inicio de sesión"""
    __gtype_name__ = 'LoginBox'

    e_host = Gtk.Template.Child()
    e_usuario = Gtk.Template.Child()
    e_passwd = Gtk.Template.Child()
    ex_error = Gtk.Template.Child()
    l_error = Gtk.Template.Child()
    rev_error = Gtk.Template.Child()

    def __repr__(self):
        return '<LoginBox>'

    def __init__(self):
        """Constructor"""
        super().__init__()

    def show_error(self):
        self.rev_error.set_reveal_child(True)

    @Gtk.Template.Callback()
    def on_changed(self, widget):
        self.rev_error.set_reveal_child(False)

    @GObject.Property
    def host(self):
        """Get the value setted on the e_host entry

        :return: String on e_host entry
        :rtype: String"""
        return self.e_host.get_text()

    @host.setter
    def host(self, host):
        """Set text to e_host

        :param host: Text for e_host
        :type host: String"""
        self.e_host.set_text(host)

    @GObject.Property
    def user(self):
        """Get the value setted on the e_usuario entry

        :return: String on e_usuario entry
        :rtype: String"""
        return self.e_usuario.get_text()

    @user.setter
    def user(self, user):
        """Set text to e_user

        :param host: Text for e_user
        :type host: String"""
        self.e_user.set_text(host)

    @GObject.Property
    def passwd(self):
        """Get the value setted on the e_passwd entry

        :return: String on e_passwd entry
        :rtype: String"""
        return self.e_passwd.get_text()

    @passwd.setter
    def passwd(self, passwd):
        """Set text to e_passwd

        :param passwd: Text for e_passwd
        :type passwd: String"""
        self.e_passwd.set_text(passwd)

    @GObject.Property(type=str)
    def errors(self):
        """Obtiene los mensajes de error

        :return: Mensajes de error.
        :rtype: String"""
        return self.ex_error.get_label()

    @errors.setter
    def errors(self, error):
        """Establece los mensajes de error

        :param error: Mensaje primario
        :type error: String."""
        self.ex_error.set_label(error)

    @GObject.Property(type=str)
    def error_detail(self):
        """Obtiene los mensajes de error

        :return: Mensajes de error.
        :rtype: String"""
        return self.l_error.get_text()

    @error_detail.setter
    def error_detail(self, error):
        """Establece los mensajes de error

        :param error: Mensaje primario
        :type error: String."""
        self.l_error.set_label(error)
