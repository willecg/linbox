# application.py
#
# Main control of the application.
# Copyright (C) 2019-2020  William Ernesto Cárdenas Gómez <willecg@cpapaseo.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gio, GObject

from routeros.window import ApplicationWindow
from routeros.enums import Estados
from routeros.settings import Settings
import threading


class Application(Gtk.Application):
    """Clase principal de la aplicación.

    ..autoclass::
        :type version: String
        :param appname: Nombre de la aplicación.
        :type appname: String

    Manipula la interfaz gráfica."""

    def __repr__(self):
        return '<Application>'

    def __init__(self, version, appname,):
        """Contructor"""
        Gtk.Application.__init__(self, application_id=appname,
                         flags=Gio.ApplicationFlags.FLAGS_NONE)
        self.connection = None
        self.api = None
        self.version = version
        self.app = appname
        self._window = None
        self._estado = Estados.LOGIN
        self.hilo_updates = None
        self._settings = Settings()

        actions = [
            ('about', self.on_about, '<Ctrl>a'),
            ('quit', self.on_quit, '<Ctrl>q'),
        ]
        for action, func, accel in actions:
            new_action = Gio.SimpleAction.new((action), None)
            new_action.connect('activate', func)
            self.add_action(new_action)
            if accel != None:
                self.set_accels_for_action('app.' + action, (accel,))
        self.connect('window-removed', self.on_quit)

    def do_startup(self):
        """Prepara la aplicación"""
        Gtk.Application.do_startup(self)

    def do_activate(self):
        """Inicia la aplicación"""
        self.props.register_session = True

        if not self._window:
            self._window = ApplicationWindow(self)
            self.bind_property('estado', self._window, 'estado',
                GObject.BindingFlags.BIDIRECTIONAL | 
                GObject.BindingFlags.SYNC_CREATE)
            self._window.present()

    def on_quit(self, action, param):
        """Tareas necesarias al teminar la ejecución de la aplicación"""
        self.props.estado = Estados.CERRANDO
        self.quit()

    def on_about(self, action, param):
        """Action On About"""
        builder = Gtk.Builder.new_from_resource(
            '/com/cpapaseo/linbox/about.ui'
        )
        about = builder.get_object('about')
        about.set_version('Versión: ' + self.version)
        about.set_name(self.app)
        about.set_transient_for(self._window)

    @property
    def version(self):
        """Obtiene la versión de la aplicación.

        :return: Versión de la aplicación.
        :rtype: String."""
        return self._version

    @version.setter
    def version(self, version):
        """Establece la versión de la aplicación.

        :param version: Versión de la aplicación
        :type version: String."""
        self._version = version

    @property
    def app(self):
        """Obtiene el nombre de la aplicación.

        :return: Nombre de la aplicación.
        :rtype: String"""
        return self._app

    @app.setter
    def app(self, app):
        """Establece el nombre de la aplicación.

        :param app: Nombre de la aplicación.
        :type app: String"""
        self._app = app

    @GObject.Property(type=int, default=int(Estados.LOGIN))
    def estado(self):
        """Obitene si el estado de la aplicación.
        
        :return: El estado de la aplicación
        :rtype: routeros.enums.Estados"""
        return self._estado

    @estado.setter
    def estado(self, estado):
        """Establece el estado de la aplicación

        :param logedin: Estado de la aplicación.
        :type logedin: Boolean"""
        self._estado = estado
