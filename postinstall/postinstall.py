#!/usr/bin/env python3

from os import environ, path
from subprocess import call

prefix = environ.get('MESON_INSTALL_PREFIX', '/usr/local')
datadir = path.join(prefix, 'share')
destdir = environ.get('DESTDIR', '')

if not destdir:
    print('Actualizando caché de íconos')
    call(['gtk-update-icon-cache', '-qtf', path.join(datadir, 'icons',
        'hicolor')])

    print('Actualizando la base de datos de escritorio')
    call(['update-desktop-database', '-q', path.join(datadir, 'applications')])

    print('Compilando de gschemas')
    call(['glib-compile-schemas', path.join(datadir, 'glib-2.0', 'schemas')])
