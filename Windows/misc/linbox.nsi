﻿; File encoding 'UTF-8 with BOM'

Unicode true
!include "MUI2.nsh"

Name "Linbox"
OutFile "Linbox.exe"
SetCompressor /final /solid lzma
SetCompressorDictSize 32

!define myAppName "Linbox"

InstallDir "$PROGRAMFILES\Linbox"
InstallDirRegKey HKCU "Software\Linbox" ""
RequestExecutionLevel admin

Var StartMenuFolder

!define MUI_ICON "..\misc\install.ico"
!define MUI_UNICON "..\misc\uninstall.ico"
!define MUI_HEADERIMAGE
!define MUI_HEADERIMAGE_BITMAP "..\misc\nsis_header.bmp"
!define MUI_WELCOMEFINISHPAGE_BITMAP "..\misc\nsis_wizard.bmp"
!define MUI_UNWELCOMEFINISHPAGE_BITMAP "..\misc\nsis_wizard.bmp"
!define MUI_COMPONENTSPAGE_SMALLDESC
!define MUI_ABORTWARNING

!insertmacro MUI_PAGE_WELCOME
!insertmacro MUI_PAGE_LICENSE "..\..\COPYING"
!insertmacro MUI_PAGE_COMPONENTS
!insertmacro MUI_PAGE_DIRECTORY
!define MUI_STARTMENUPAGE_REGISTRY_ROOT "HKCU"
!define MUI_STARTMENUPAGE_REGISTRY_KEY "Software\Linbox"
!define MUI_STARTMENUPAGE_REGISTRY_VALUENAME "Start Menu Folder"
!insertmacro MUI_PAGE_STARTMENU Application $StartMenuFolder
!insertmacro MUI_PAGE_INSTFILES
!define MUI_FINISHPAGE_RUN "$INSTDIR\bin\Linbox.exe"
!insertmacro MUI_PAGE_FINISH

!insertmacro MUI_UNPAGE_WELCOME
!insertmacro MUI_UNPAGE_CONFIRM
!insertmacro MUI_UNPAGE_INSTFILES
!insertmacro MUI_UNPAGE_FINISH

;Show all languages, despite user's codepage
!define MUI_LANGDLL_ALLLANGUAGES

!insertmacro MUI_LANGUAGE "English"
!insertmacro MUI_LANGUAGE "Spanish"
!insertmacro MUI_RESERVEFILE_LANGDLL

; English
LangString NAME_Languages ${LANG_ENGLISH} "Languages"
LangString NAME_SecDesktopIcon ${LANG_ENGLISH} "Create desktop icon."
LangString DESC_SecLinbox ${LANG_ENGLISH} "Install Linbox files."
LangString DESC_SecDesktopIcon ${LANG_ENGLISH} "If you agree, an shortcut will be created on your desktop."
LangString STR_Installed ${LANG_ENGLISH} "Looks like Linbox is already installed. Uninstlall it?"
LangString STR_Running ${LANG_ENGLISH} "Linbox in running.$\n\
		Please quit Linbox and restart the uninstaller."

; Español
LangString NAME_Languages ${LANG_SPANISH} "Idiomas"
LangString NAME_SecDesktopIcon ${LANG_SPANISH} "Crear icono de escritorio."
LangString DESC_SecLinbox ${LANG_SPANISH} "Instala los archivos de Linbox."
LangString DESC_SecDesktopIcon ${LANG_SPANISH} "De aceptarlo, un acceso directo aparecerá en su escritorio."
LangString STR_Installed ${LANG_SPANISH} "Aparentemente, Linbox ya esta instalado. Desinstalarlo?"
LangString STR_Running ${LANG_SPANISH} "Parece que Linbox se esta ejecutando.$\n\
		Por favor salga de Linbox y vuelva a ejecutar el proceso para desinstalar."

Section "Linbox" SecLinbox
	SectionIn RO
	
    Var /GLOBAL arch_name
    StrCpy $arch_name "(64-Bit)"
    StrCmp ${ARCH} "mingw64" cont
    StrCpy $arch_name "(32-Bit)"
    cont:
	
	SetOutPath "$INSTDIR"
	File /r "${ARCH}\*.*"

	WriteRegStr HKCU "Software\Linbox" "" $INSTDIR
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Linbox" "DisplayName" "Linbox ${VERSION} $arch_name"
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Linbox" "UninstallString" "$INSTDIR\Uninstall.exe"
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Linbox" "DisplayIcon" "$INSTDIR\bin\Linbox.exe"
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Linbox" "DisplayVersion" "${VERSION}"
	WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Linbox" "URLInfoAbout" "https://www.cpapaseo.com/"
	WriteUninstaller "$INSTDIR\Uninstall.exe"

	SetOutPath "$INSTDIR\bin"
	!insertmacro MUI_STARTMENU_WRITE_BEGIN Application
		SetShellVarContext current
		CreateDirectory "$SMPROGRAMS\$StartMenuFolder"
		CreateShortCut "$SMPROGRAMS\$StartMenuFolder\Linbox.lnk" "$INSTDIR\bin\Linbox.exe"
		SetShellVarContext all
		CreateDirectory "$SMPROGRAMS\$StartMenuFolder"
		CreateShortCut "$SMPROGRAMS\$StartMenuFolder\Linbox.lnk" "$INSTDIR\bin\Linbox.exe"
	!insertmacro MUI_STARTMENU_WRITE_END

SectionEnd

Section $(NAME_SecDesktopIcon) SecDesktopIcon
	SetShellVarContext current
	SetOutPath "$INSTDIR\bin"
	CreateShortCut "$DESKTOP\Linbox.lnk" "$INSTDIR\bin\Linbox.exe"
SectionEnd

Section "Uninstall"
	RMDir /r "$INSTDIR"

	!insertmacro MUI_STARTMENU_GETFOLDER Application $StartMenuFolder

	SetShellVarContext current
	Delete "$SMPROGRAMS\$StartMenuFolder\Linbox.lnk"
	RMDir "$SMPROGRAMS\$StartMenuFolder"
	Delete "$DESKTOP\Linbox.lnk"
	Delete "$SMSTARTUP\Linbox.lnk"
	SetShellVarContext all
	Delete "$SMPROGRAMS\$StartMenuFolder\Linbox.lnk"
	RMDir "$SMPROGRAMS\$StartMenuFolder"

	DeleteRegKey /ifempty HKCU "Software\Linbox"
	DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Linbox"
SectionEnd

!insertmacro MUI_FUNCTION_DESCRIPTION_BEGIN
	!insertmacro MUI_DESCRIPTION_TEXT ${SecLinbox} $(DESC_SecLinbox)
	!insertmacro MUI_DESCRIPTION_TEXT ${SecDesktopIcon} $(DESC_SecDesktopIcon)
!insertmacro MUI_FUNCTION_DESCRIPTION_END

Function un.onInit
;	Check that Linbox is not running before uninstalling
	FindWindow $0 "gdkWindowToplevel" "Linbox"
	StrCmp $0 0 Remove
	MessageBox MB_ICONSTOP|MB_OK $(STR_Running)
	Quit
Remove:
	!insertmacro MUI_UNGETLANGUAGE
FunctionEnd

Function .onInit
	BringToFront
;	Check if already running
;	If so don't open another but bring to front
	System::Call "kernel32::CreateMutexA(i 0, i 0, t '$(^Name)') i .r0 ?e"
	Pop $0
	StrCmp $0 0 launch
	StrLen $0 "$(^Name)"
	IntOp $0 $0 + 1
	FindWindow $1 '#32770' '' 0 $1
	IntCmp $1 0 +3
	System::Call "user32::ShowWindow(i r1,i 9) i."         ; If minimized then maximize
	System::Call "user32::SetForegroundWindow(i r1) i."    ; Bring to front
	Abort

launch:
;	Check to see if old install (inno setup) is already installed
	ReadRegStr $R0 HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\Linbox_is1" "UninstallString"
;	remove first and last " char
	StrLen $0 $R0
	IntOp $0 $0 - 2
	strcpy $1 $R0 $0 1
	IfFileExists $1 +1 NotInstalled
	MessageBox MB_YESNO|MB_DEFBUTTON2|MB_TOPMOST $(STR_Installed) IDNO Quit
	StrCmp $R1 2 Quit +1
	ExecWait '$R0 _?=$INSTDIR' $R2
	StrCmp $R2 0 +1 Quit

NotInstalled:	
;	Check to see if new installer (NSIS)already installed
	ReadRegStr $R3 HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\Linbox" "UninstallString"
	IfFileExists $R3 +1 ReallyNotInstalled
	MessageBox MB_YESNO|MB_DEFBUTTON2|MB_TOPMOST $(STR_Installed) IDNO Quit
	StrCmp $R4 2 Quit +1
	ExecWait '$R3 _?=$INSTDIR' $R5
	StrCmp $R5 0 ReallyNotInstalled Quit
Quit:
	Quit
 
ReallyNotInstalled:
	!insertmacro MUI_LANGDLL_DISPLAY
FunctionEnd
