# Makes simple operations on mikrotik routers.
# Copyright (C) 2019-2020  William Ernesto Cárdenas Gómez <willecg@cpapaseo.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import os
import sys
import signal
import argparse
import gi
gi.require_version('Gtk','3.1.1')
gi.require_version('GIRepository', '2.0')
from gi.repository import GIRepository, Gio
VERSION = '3.1.0'
APPID = 'com.cpapaseo.linbox'
pkgdatadir = '../share/com.cpapaseo.linbox'

sys.path.insert(1, pkgdatadir)

signal.signal(signal.SIGINT, signal.SIG_DFL)

def set_parser():
        """Interpreta la entrada de línea de comandos."""
        parser = argparse.ArgumentParser(
            description = 'Activa y desactiva VPNs',
            prog = APPID
        )
        parser.add_argument('-v', '--version',
            action='version',
            version='%(prog)s: v' + VERSION + 
            ' (c) 2019 William Ernesto Cárdenas Gómez'
        )
        args = parser.parse_args()


def set_resources():
    """Carga el archivo con los resources"""
    resource = Gio.Resource.load(os.path.join(pkgdatadir, 'linbox.gresource'))
    resource._register()


def run_application():
    """Ejecuta la aplicación"""
    from routeros.application import Application

    app = Application(VERSION, APPID)
    return app.run(sys.argv)


def main():
    set_parser()
    set_resources()
    sys.exit(run_application())
